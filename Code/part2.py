from sklearn.cluster import KMeans
import pandas as pd
import matplotlib.pyplot as plt

path = "/Users/quentinroustan/PycharmProjects/quentin-yashomati/Data/part1.csv"
df = pd.read_csv(path, index_col="CLIENT_ID")

x = df[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]]
print(x.head())

# Clustering with KMeans
kmeans = KMeans(n_clusters=4).fit(x)
labels = kmeans.labels_
print(x.head())
new_df = pd.DataFrame(x)
print(new_df.head())
new_df["label_kmeans"] = labels  # Use of Kmeans labels to create a new column assigning label number
new_df = new_df.rename(columns={'FIRST_COURSE': 'First', 'SECOND_COURSE': 'Second', 'THIRD_COURSE': 'Third'})

print(new_df.head())
# Creation of the graphical 3D representation of the Kmean clustering
fig = plt.figure(figsize=(20, 10))
ax = fig.add_subplot(111, projection="3d")
# Assign the labels, colors and options for the graphical part
ax.scatter(new_df["First"][new_df.label_kmeans == 0], new_df["Second"][new_df.label_kmeans == 0],
           new_df["Third"][new_df.label_kmeans == 0], c="blue", edgecolor="w", s=100, linestyle="-")
ax.scatter(new_df["First"][new_df.label_kmeans == 1], new_df["Second"][new_df.label_kmeans == 1],
           new_df["Third"][new_df.label_kmeans == 1], c="red", edgecolor="w", s=100, linestyle="-")
ax.scatter(new_df["First"][new_df.label_kmeans == 2], new_df["Second"][new_df.label_kmeans == 2],
           new_df["Third"][new_df.label_kmeans == 2], c="green", edgecolor="w", s=100, linestyle="-")
ax.scatter(new_df["First"][new_df.label_kmeans == 3], new_df["Second"][new_df.label_kmeans == 3],
           new_df["Third"][new_df.label_kmeans == 3], c="orange", edgecolor="w", s=100, linestyle="-")

centers = kmeans.cluster_centers_

ax.scatter(centers[:, 0], centers[:, 1], centers[:, 2], c="black", s=500)
plt.xlabel("First")
plt.ylabel("Second")
ax.set_zlabel("Third")
ax.legend(['0', '1', '2', '3'], fontsize=12)

plt.show()
# Export to csv
new_df.to_csv("df_kmeans.csv")
