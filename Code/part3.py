import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Load the Data
path = "https://bitbucket.org/gedegeyterTSM/examtsm2022/raw/2ecc49b3bc3b02ab01d68b1bd927bda44986eb0b/Data/part3.csv"
path_2 = "/Users/quentinroustan/PycharmProjects/quentin-yashomati/Data/df_kmeans.csv"
df_actual = pd.read_csv(path, index_col="CLIENT_ID")
df_result = pd.read_csv(path_2, index_col="CLIENT_ID")
print(df_actual.head())
print(df_result.head())

# Compare the result obtained with the actual labels
# Dividing the number of Equal value (True), by the total number of entries
accuracy = (df_result[["CLIENT_TYPE"]]==df_actual).sum()/(df_result[["CLIENT_TYPE"]]==df_actual).count()*100

print("Our labels results are ", round(accuracy[0],2) ,"% similar to the actual client type")

# Can you determine the distribution of clients (how many of each group do I expect)
# So answering the question “what type of client is this"

sns.displot(df_result["CLIENT_TYPE"],bins=200,stat="probability")
plt.show()
print("Clients distribution :","\n", df_result['CLIENT_TYPE'].value_counts(normalize=True) * 100)

# Can you determine the likelihood for each of these clients to get a certain course.
# So answering the question “How likely is this type of client to get a starter, main and dessert?”
# Using loop
for i in (df_result["CLIENT_TYPE"].unique()):
    data = df_result[df_result["CLIENT_TYPE"]==i][["First","Second","Third"]]
    size=data.count()
    print(i, "Group", "\n",(data!=0).sum()/size*100)

# Second Method
df_withna=df_result.replace(0,np.nan)
print(df_withna.head())
distribution = df_withna.groupby("CLIENT_TYPE").apply(lambda x: x.notna().mean()).drop("CLIENT_TYPE",axis=1)*100
print("Likelihood for each client type to gert a certain course :", "\n",distribution)

# Can you figure out the probability of a certain type of customer ordering a certain dish
# For example: a one-time customer buys ice cream in 30% cases and pie in 70% case (if they have dessert)

df_item = pd.read_csv("/Users/quentinroustan/PycharmProjects/quentin-yashomati/Data/df_menu_item.csv",index_col="CLIENT_ID")
df_item= df_item[["starter","main","dessert"]]
df_item=df_item.replace(0,"Nothing")
joined_df = df_item.join(df_result["CLIENT_TYPE"]) #We Join the client type data with the df we output earlier containing the dishes choice

df_starter = joined_df.groupby(["CLIENT_TYPE","starter"]).size().unstack()
df_starter = df_starter.div(df_starter.sum(axis=1), axis=0) #To get the probability
print(df_starter)
df_main = joined_df.groupby(["CLIENT_TYPE","main"]).size().unstack()
df_main = df_main.div(df_main.sum(axis=1), axis=0)
print(df_main)
df_dessert = joined_df.groupby(["CLIENT_TYPE","dessert"]).size().unstack()
df_dessert = df_dessert.div(df_dessert.sum(axis=1), axis=0)
print(df_dessert)

# Determine the distribution of dishes, per course, per customer type.

df_starter.plot(kind="bar")
df_main.plot(kind="bar")
df_dessert.plot(kind="bar")
plt.show()

# Can you determine the distribution of the cost of the drinks per course?
# To check this last part again
joined_df.groupby(["CLIENT_TYPE", "starter", "first_drink"]).size().unstack.plot(kind="bar")
joined_df.groupby(["CLIENT_TYPE", "main", "second_drink"]).size().unstack.plot(kind="bar")
joined_df.groupby(["CLIENT_TYPE", "dessert", "third_drink"]).size().unstack.plot(kind="bar")

#or using
pd.pivot_table(joined_df, index='CLIENT_TYPE', columns='FIRST_COURSE', values='First_drink').plot(kind='bar')
pd.pivot_table(joined_df, index='CLIENT_TYPE', columns='SECOND_COURSE', values='Second_drink').plot(kind='bar')
pd.pivot_table(joined_df, index='CLIENT_TYPE', columns='THIRD_COURSE', values='Third_drink').plot(kind='bar')