import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plotly.express as px

path = "/Users/quentinroustan/PycharmProjects/quentin-yashomati/Data/part1.csv"

df = pd.read_csv(path, index_col="CLIENT_ID")

# Display an overview of the dataframe and some basic statistics
print(df.head())
print(df.describe())

# Question 1 : Make a plot of the distribution of the cost in each course

# following code not working yet
fig = px.histogram(df, x="TIME", y=["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"], title="Distribution of cost per time")
fig.show()

# Question 2 : Make a bar plot of the cost per course

# Plot of the distribution of the cost per course, using subplots
# Subplots
df.plot(subplots=True, layout=(3, 1), figsize=(20.0, 18.0), grid=True, kind="hist", bins=30)
plt.show()

# Same plot
df[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']].plot.hist(bins=15, figsize=(15, 10), grid=True, alpha=0.8)
plt.show()


# Average cost per course
df.mean().plot(kind="bar")
plt.show()

# Question 3 : Determine the cost of the drinks per course

# Creation of lists that contains the price for the three different menu
menu_1 = [3, 15, 20]
menu_2 = [9, 20, 25, 40]
menu_3 = [15, 10]


# Function that take as an input a column of the dataframe (one of the three course) and the menu related to this
# course (1,2,3)
# Find the minimal value based on the difference between the actual course price and each dish price of the same course
# to extract the drink price
def menu_item(df, menu):
    drink = []
    dish_price = []
    for i in df.values:
        if i == 0:
            drink_price = 0
            drink.append(drink_price)
            dish_price.append(0)
        # Case if price of the menu is equal to the price of one element of the list (therefore no drinks ordered)
        elif i in menu:
            drink_price = 0
            dish_price.append(i)
        else:
            drink_price = 1000000
            for j in menu:
                if i - j > 0:
                    drink_price = min(abs(i - j), drink_price)
            drink.append(drink_price)
            dish_price.append(i - drink_price)

    return drink, dish_price


# Return the decomposition of the menu : the price of the drink and the prices of the dishes in two separate list

#print(menu_item(df["FIRST_COURSE"], menu_1))

# Add column for the drink price
df["menu_item"] = menu_item(df["FIRST_COURSE"], menu_1)[0]
df["second_drink"] = menu_item(df["SECOND_COURSE"], menu_2)[0]
df["third_drink"] = menu_item(df["THIRD_COURSE"], menu_3)[0]

# Add column for the dish price
df["first_dish_price"] = menu_item(df["FIRST_COURSE"], menu_1)[1]
df["second_dish_price"] = menu_item(df["SECOND_COURSE"], menu_2)[1]
df["third_dish_price"] = menu_item(df["THIRD_COURSE"], menu_3)[1]

# Create dictionaries to store the names and prices of the dishes and then map it to the dataframe to display the name
starters_dic = {3: "Soup", 15: "Tomato-Mozarella", 20: "Oysters", 0: 0}
mains_dic = {9: "Salad", 20: "Spaghetti", 40: "Lobster", 25: "Steak", 0: 0}
desserts_dic = {15: "Ice cream", 10: "Pie", 0: 0}

# Replace the price column in the dataframe by the name of the dish
df["starter"] = df["first_dish_price"].map(starters_dic)
df["main"] = df["second_dish_price"].map(mains_dic)
df["dessert"] = df["third_dish_price"].map(desserts_dic)

print(df.head())

# Export Dataframe to CSV for latter usage
df.to_csv("/Users/quentinroustan/PycharmProjects/quentin-yashomati/Data/df_menu_item.csv")
