import random as rd
import csv
import pandas as pd


class Onetime_client(object):
    # All item from the menu
    menu = [["Nothing", "Soup", "Tomato-Mozarella", "Oysters"], ["Salad", "Spaghetti", "Steak", "Lobster"],
            ["Nothing", "Ice cream", "Pie"]]
    # Distribution for each type determined in previous part
    distrib = [[0.91, 0.9, 0, 0], [0, 0.19, 0.72, 0.09], [0.85, 0.01, 0.14]]

    def __init__(self, _ID):
        self.ID = _ID

    # Function created to initialise client choices, the dishes it will pick for each course, and if it will be
    # returning or not
    def goToRestaurant(self): 
        courses = []
        for i in range(len(menu)):
            courses.append(
                rd.choices(self.menu[i], self.distrib[i])[0])
        # One time client never return
        returning = False
        choices = [returning, courses]
        return choices


class Business_client(Onetime_client):
    distrib = [[0.23, 0.09, 0.04, 0.64], [0, 0, 0, 1], [0.12, 0.09, 0.79]]

    def goToRestaurant(self):
        courses = []
        for i in range(len(menu)):
            courses.append(rd.choices(self.menu[i], self.distrib[i])[0])
        returning = rd.choices([True, False], [50, 50])[0]
        choices = [returning, courses]
        return choices


class Healthy_client(Onetime_client):
    distrib = [[0.04, 0.96, 0, 0], [1, 0, 0, 0], [0.93, 0.01, 0.06]]

    def goToRestaurant(self):
        courses = []
        for i in range(len(menu)):
            courses.append(rd.choices(self.menu[i], self.distrib[i])[0])
        returning = rd.choices([True, False], [70, 30])[0]
        choices = [returning, courses]
        return choices


class Retirement_client(Onetime_client):
    distrib = [[0, 0.01, 0.32, 0.67], [0.17, 0.59, 0.24, 0], [0.05, 0.1, 0.85]]

    def goToRestaurant(self):
        courses = []
        for i in range(len(menu)):
            courses.append(rd.choices(self.menu[i], self.distrib[i])[0])
        returning = rd.choices([True, False], [90, 10])[0]
        choices = [returning, courses]
        return choices


client_list = []
daily_dish = 20  # Number of dish in a day as set in the question
menu_items = {
    "Nothing": 0,
    "Soup": 3.0,
    "Tomato-Mozarella": 15.0,
    "Oysters": 20.0,
    "Salad": 9.0,
    "Spaghetti": 20.0,
    "Steak": 25.0,
    "Lobster": 40.0,
    "Ice cream": 15.0,
    "Pie": 10.0
}  # Dictionnary to link name to price


def count_clients_type(client_list):  # Counting the number of returning client type in a list
    nb_client_type = {
        "Business_client": 0,
        "Healthy_client": 0,
        "Retirement_client": 0
    }
    for i in client_list:
        client_type = type(i).__name__  # Return the class or subclass name
        nb_client_type[client_type] += 1

    return nb_client_type


def simulation(_ID, incoming_client):
    ID = _ID
    client_list = incoming_client
    nb_client_type = count_clients_type(incoming_client)
    future_incoming_client = []
    # Adding all clients for the day in the right proportion

    # We try to respect the distribution of client by filling the remaining returning client number per type
    for i in range(round(daily_dish * 15.2 / 100) - nb_client_type["Business_client"]):
        client_list.append(Business_client(ID))
        ID += 1
    for i in range(round(daily_dish * 15.04 / 100) - nb_client_type["Retirement_client"]):
        client_list.append(Retirement_client(ID))
        ID += 1
    for i in range(round(daily_dish * 21.6 / 100) - nb_client_type["Healthy_client"]):
        client_list.append(Healthy_client(ID))
        ID += 1
    while len(client_list) < 20:
        client_list.append(Onetime_client(ID))
        ID += 1
    # Then for each client in the client list for the day, we use the function defined before to make his choices
    #
    for client in client_list:
        time = rd.choices(["Lunch", "Dinner"], [0.5, 0.5])
        #For each client add their respective information
        returning = client.goToRestaurant()[0]
        dishes = client.goToRestaurant()[1]
        course_price = [menu_items[dishes[0]], menu_items[dishes[1]], menu_items[dishes[2]]]
        drink_price = [rd.uniform(0, 2), rd.uniform(0, 5), rd.uniform(0,5)]
        # Max price of each course is the minimal different between to dish inside the same course
        totals = [course_price[0] + drink_price[0], course_price[1] + drink_price[1], course_price[2] + drink_price[2]]
        client_type = type(client).__name__ #Get the name of the class that correspond to the client type
        client_info = [time[0], client.ID, client_type, dishes[0], dishes[1],
                       dishes[2], drink_price[0], drink_price[1], drink_price[2], totals[0], totals[1], totals[2]]
        entries.append(client_info)

        if returning == True:
            future_incoming_client.append(client)

    return future_incoming_client
# Return the lsit of the returning client, that will be looped latter to have a constant database running

# Creation of the list with all the simulation entries
daily_dish = 20
ID=0
client_list=[]
entries=[]
# Number of year
for i in range(5):
    # Number of day in a year
    for i in range(365):
        future_incoming_client = simulation(ID, client_list)
        ID = ID + 20
        client_list = future_incoming_client


# How would the revenue of the restaurant change if they suddenly had twice more healthy customers?
# We just have to run the script again by changing the 21.6 value in the loop by 43,2 (twice) and compare to total profit
# Some of both TOTAL accross all the dataframe

# TOTAL PROFIT
profit_basic=df["TOTAL1"].sum()[0]+df["TOTAL2"].sum()[0]+df["TOTAL3"].sum()[0]

# Compare to profit_twice
# Profit_basic = 1436039.77
# Profit twice(with twice healthy client) = 1292497.74
# Drop in -9,99%

# What if they increased the price of the Spaghetti?
# We can do the same by changing the Spaghetti price
# IF we increase by 10% the spaghetti price :
#TOTAL PROFIT AFTER : 1448688.83
# It represent a 0,88% increase
