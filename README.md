This project is done by 
1.Quentin Roustan 2.Peruman Yashomati 

DESCRIPTION OF THE PROJECT OF RESTAURANT SIMULATION
Each part of the answer is split into 4 parts: 

    1. PART 1 - In this part we are performing an Exploratory data analysis 
    Python file - part1.py; Data used - part1.csv; Libraries used - pandas, numpy, matplotlib.pyplot, plotly.express; 
    Output of code - Histogram_distibution of cost per time, Histogram_distribution of the cost per course, 
    Price and drink in different columns, df_menu_item.csv;
    
    2. PART 2 - In this part data are clustered using Kmeans
    Python file - part2.py; Data used - part1.csv;  Libraries used - KMeans, pandas, matplotlib.pyplot;
    Output of code - Kmeans_cluster.png, Clustering.txt, df_kmeans.csv
    
    3. Part 3 - In this part we are determining the distributions
    Python file - part3.py; Data used - part3.csv, df_kmeans.csv ; Libraries used - seaborn, pandas, matplotlib.pyplot;
    Output of code - Histogram_type of clients, Barplot_probability of a certain type of customer ordering a certain dish, 
    Barplot_distribution of dishes, per course, per customer type;
    
    4. Part 4 - Restaurant Simulation
    Python file - part4.py; Data used - all datas from part 1 to 3; Libraries used - random, csv, pandas
    Output of code - Finding future coming clients and the profit through the restaurant simulation